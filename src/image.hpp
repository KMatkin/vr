#pragma once

#include "stb_image.h"
#include <array>
#include <iostream>
#include <stdlib.h>
#include <string>

template <size_t C> struct image {
  using component = unsigned char;

  unsigned char *data;
  int height;
  int width;
  const unsigned int channels = C;
  unsigned int loaded_channels;

  static const component max_component_value = 255.0f;

  image(const std::string &path) {
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(path.c_str(), &width, &height,
                                    reinterpret_cast<int *>(&loaded_channels),
                                    static_cast<int>(C));
    if (data == NULL) {
      std::cerr << "Couldn't load image at " << path << std::endl;
    }
    if (loaded_channels != this->channels) {
      std::cerr << "Loaded channels in image not equal desired channels "
                << path << " wanted " << C << " loaded " << loaded_channels
                << std::endl;
    }
    this->data = data;
  }

  std::array<component, C>
  operator[](const std::pair<size_t, size_t> &index) const {
    std::array<component, C> res;
    for (size_t i = 0; i < C; i++) {
      res[i] = data[index.first * width * C + index.second * C + i];
    }
    return res;
  }

  component grey(const std::pair<size_t, size_t> &index) const {
    auto components = this->operator[]({index.first, index.second});
    unsigned int componentsSum = 0;
    for (const auto &c : components) {
      componentsSum += c;
    }
    return static_cast<float>(componentsSum) / components.size();
  }

  ~image() { delete[] data; }
};

using Image = image<3>;