#pragma once
#include <glad/glad.h>
#include "shader.hpp"
#include <exception>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <optional>

struct program_failure : std::runtime_error {
  using std::runtime_error::runtime_error;
};

struct program {

  GLint pointer = 0;

  template <GLuint T1, GLuint T2>
  program(const shader<T1> &shader1, const shader<T2> &shader2) {
    pointer = glCreateProgram();
    glAttachShader(pointer, shader1.pointer);
    glAttachShader(pointer, shader2.pointer);
    glLinkProgram(pointer);
    GLint success;
    char infoLog[512];
    glGetProgramiv(pointer, GL_LINK_STATUS, &success);
    if (!success) {
      glGetProgramInfoLog(pointer, 512, NULL, infoLog);
      throw program_failure{"Error: couldn't link program"};
    }
  }

  program() : pointer(0) {}

  ~program() {
    // glDeleteProgram(this->pointer); // TODO: Gracefully delete program when
    // context is dead
  }

  inline void use() const { glUseProgram(pointer); }

  void setMat4(const std::string &name, const glm::mat4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(pointer, name.c_str()), 1, GL_FALSE,
                       glm::value_ptr(mat));
  }

  void setInt(const std::string &name, int value) const {
    glUniform1i(glGetUniformLocation(pointer, name.c_str()), value);
  }

  void setInt(const int &handler, int value) const {
    glUniform1i(handler, value);
  }

  void setVec4(const std::string &name, const glm::vec4 &vec) const {
    glUniform4f(glGetUniformLocation(pointer, name.c_str()), vec[0], vec[1], vec[2], vec[3]);
  }

  void setVec3(const std::string &name, const glm::vec3 &vec) const {
    glUniform3f(glGetUniformLocation(pointer, name.c_str()), vec[0], vec[1], vec[2]);
  }
};
