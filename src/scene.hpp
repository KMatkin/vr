#pragma once

#include <memory>
#include <vector>
#include <glm/glm.hpp>
#include "camera.hpp"
#include "stereocamera.hpp"
#include "skybox.hpp"
#include "gameobject.hpp"
#include "entitymanager.hpp"
#include "screen.hpp"
#include "pointer.hpp"

struct Selection {
  unsigned int objectID = 0;
  std::shared_ptr<GameObject> object = nullptr;
};

struct Scene {
  std::unique_ptr<StereoCamera> camera;
  std::unique_ptr<Skybox> skybox;
  std::vector<std::shared_ptr<GameObject>> objects;
  std::unique_ptr<EntityManager> entityManager;
  std::shared_ptr<Screen> screen;
  std::unique_ptr<Pointer> pointer;
  glm::vec3 lightPos;
  Selection selection;

  bool mousePressed = false;
  bool shouldHitTest = false;
  int lastYState;
  bool stereo = false;

  Scene(std::shared_ptr<Screen> screen) {
    this->screen = screen;
  }

  template<typename T>
  inline void addObject(T &&obj) {
    objects.push_back(std::static_pointer_cast<GameObject>(obj));
  }

  void update() {
    selection.object = entityManager->getObject(selection.objectID);
  }

  bool processInput(GLFWwindow *window) {
    camera->processInput(window);
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS && !mousePressed) {
      mousePressed = true;
      shouldHitTest = true;
    }
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) {
      mousePressed = false;
    }
    if (glfwGetKey(window, GLFW_KEY_HOME) == GLFW_PRESS) {
      camera->convergence += 1;
      std::cout << "convergence " << camera->convergence << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_END) == GLFW_PRESS) {
      camera->convergence -= 1;
      std::cout << "convergence " << camera->convergence << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS) {
      camera->eyeSeparation += 0.01;
      std::cout << "eye separation " << camera->eyeSeparation << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS) {
      camera->eyeSeparation -= 0.01;
      std::cout << "eye separation " << camera->eyeSeparation << std::endl;
    }
    int yState = glfwGetKey(window, GLFW_KEY_Y);
    if (yState < GLFW_PRESS && yState != lastYState) {
      stereo = !stereo;
    }
    lastYState = yState;
    if (selection.object != nullptr) {
      selection.object->processInput(window);
    }
    return false;
  }

  void hitTestRender(glm::mat4 &projection, glm::mat4 &view) {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (const auto &obj: objects) {
      obj->hitTestRender = true;
      obj->render(projection, view, glm::vec3(0.0), glm::vec3(0.0));
      obj->hitTestRender = false;
    }
    glFlush();
    glFinish();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    GLubyte buffer[4];
    glReadPixels(
      screen->width / 2, 
      screen->height / 2,
      1, 1,
      GL_RGBA,
      GL_UNSIGNED_BYTE, 
      buffer
    );
    unsigned int id = buffer[0] + buffer[1] * 0xFF + buffer[2] * 0xFFFF;
    selection.objectID = id; 
    shouldHitTest = false;
  }

  void renderObjects(glm::mat4 &view, glm::mat4 &projection, glm::vec3 &eyePos) {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (const auto &obj: objects) {
      obj->render(projection, view, lightPos, eyePos);
    }
    pointer->render(projection, view, lightPos, eyePos);
    skybox->render(projection, view, lightPos, eyePos);
  }

  void stereoRender() {
    auto leftProj = camera->leftFrustum();
    auto leftView = camera->stereoViewMatrix(true);
    auto eyePos = camera->position();
    glColorMask(true, false, false, false);
    renderObjects(leftView, leftProj, eyePos);
    glClear(GL_DEPTH_BUFFER_BIT);
    auto rightProj = camera->rightFrustum();
    auto rightView = camera->stereoViewMatrix(false);
    glColorMask(false, true, true, false);
    renderObjects(rightView, rightProj, eyePos);
    glColorMask(true, true, true, true);
  }

  void render() {
    if (shouldHitTest) {
      auto view = dynamic_cast<Camera*>(camera.get())->viewMatrix();
      auto projection = camera->projectionMatrix();
      hitTestRender(projection, view);
    } else if (stereo) {
      stereoRender();
    } else {
      auto view = camera->viewMatrix();
      auto proj = camera->projectionMatrix();
      auto eyePos = camera->position();
      renderObjects(view, proj, eyePos);
    }
  }
};