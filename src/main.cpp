#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <chrono>
#include <iostream>
#include <memory>
#include <string_view>
#include "debug.hpp"
#include "tools.hpp"
#include "game.hpp"

using namespace std::chrono_literals;

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void processInput(GLFWwindow *window);
void keyCallback(GLFWwindow *window, int, int, int, int);

int main(int argc, char **argv) {
  using clock = std::chrono::high_resolution_clock;

  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

  GLFWwindow *window = glfwCreateWindow(InitialWidth, InitialHeight, GameTitle.c_str(), NULL, NULL);
  if (window == NULL) {
    std::cerr << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return EXIT_FAILURE;
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetKeyCallback(window, keyCallback);

  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
    std::cerr << "Failed to initialize GLAD" << std::endl;
    return EXIT_FAILURE;
  }

  std::unique_ptr<Game> game = std::make_unique<Game>(window);
  glfwSetWindowUserPointer(window, static_cast<void*>(game.get()));

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
  glEnable(GL_DEPTH_TEST);
  // glEnable(GL_CULL_FACE);

  std::chrono::nanoseconds lag(0ns);
  auto time_start = clock::now();

  while (true) {
    auto delta_time = clock::now() - time_start;
    time_start = clock::now();
    lag += std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time);

    game->processInput();

    if (game->shouldClose) {
      break;
    }

    game->update();
    game->render();
    glfwPollEvents();
  }
  glfwTerminate();
  return EXIT_SUCCESS;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  Game* game = static_cast<Game*>(glfwGetWindowUserPointer(window));
  game->onFramebufferSizeChanged(width, height);
}

void mouse_callback(GLFWwindow *window, double xpos, double ypos) {
  Game* game = static_cast<Game*>(glfwGetWindowUserPointer(window));
  game->onMousePositionChanged(xpos, ypos);
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
  Game* game = static_cast<Game*>(glfwGetWindowUserPointer(window));
  game->onKeyModeChanged(key, scancode, action, mods);
}