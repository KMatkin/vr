#pragma once
#include <unordered_map>
#include <memory>
#include <gameobject.hpp>

class EntityManager {
public:

  inline std::shared_ptr<GameObject> getObject(unsigned int id) {
    return (id == 0) ? nullptr : objects[id];
  }

  template<typename T, typename ...Args>
  inline std::shared_ptr<T> create(Args&& ...args) {
    auto id = getNextId();
    auto instance = std::make_shared<T>(std::forward<Args>(args)..., id);
    objects.emplace(id, instance);
    return instance;
  } 

 private:
  unsigned int lastID = 0;
  std::unordered_map<unsigned int, std::shared_ptr<GameObject>> objects;

  unsigned int getNextId() {
    return ++lastID;
  }
};