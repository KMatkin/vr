#pragma once

#include <glm/glm.hpp>
#include "gameobject.hpp"
#include "program.hpp"

class Sphere : public GameObject {
public:
  Sphere(const program &p, const glm::vec3 &position, unsigned int entityID);
  virtual ~Sphere() = default;
  void render(const glm::mat4 &projectionMatrix,
              const glm::mat4 &viewMatrix,
              const glm::vec3 &lightPos,
              const glm::vec3 &eyePos) override;

  bool processInput(GLFWwindow *window) override;

private:
  program p;
  glm::vec3 m_position;
  unsigned int indices_num;
  bool m_useDisplacement;
  int m_lastTState;
};