#include "pointer.hpp" 
#include "tools.hpp"

Pointer::Pointer(program p) : GameObject(0), p(p) {
  const GLfloat vertices[] = {
    1.0, 1.0, -0.1,
    1.0, -1.0, -0.1,
    -1.0, -1.0, -0.1,
    -1.0, 1.0, -0.1
  };
  const GLuint indices[] = {
    2, 1, 0, 0, 3, 2
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);

  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, (void*)0);
}

void Pointer::render(const glm::mat4 &proj, const glm::mat4 &view, const glm::vec3 &lightPos, const glm::vec3 &eyePos) {
  const auto scaleMat = glm::scale(glm::mat4(1.0f), glm::vec3(0.001, 0.001, 1.0));
  p.use();
  p.setMat4("modelViewMatrix", scaleMat);
  p.setMat4("projectionMatrix", proj);
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}