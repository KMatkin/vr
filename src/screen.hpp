#pragma once

struct Screen {
  unsigned int width;
  unsigned int height;

  Screen(unsigned int width, unsigned int height) {
    this->width = width;
    this->height = height;
  }
};

