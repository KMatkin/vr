#include "torus.hpp"
#include "tools.hpp"
#include <map>
#include <unordered_map>


struct VecHashHelper {
  std::size_t operator()(const glm::vec3 &v) const {
    using std::hash;
    return hash<float>{}(v[0]) ^ hash<float>{}(v[1]) ^ hash<float>{}(v[2]);
  }
};

Torus::Torus(const StlModel &stlModel, const program &p,
             const glm::vec3 &position, unsigned int entityID)
    : GameObject(entityID), p(p), m_position(position) {
  using Vertex = std::array<float, 6>;
  unsigned int num_vertices = stlModel.triangles.size() * 3;
  std::vector<Vertex> vertices = std::vector<Vertex>(num_vertices);
  std::unordered_map<glm::vec3, glm::vec3, VecHashHelper> normals_map;
  int index = 0;
  for (const auto &triangle : stlModel.triangles) {
    for (const glm::vec3 &point : triangle.points()) {
      auto pointIt = normals_map.find(point);
      if (pointIt == normals_map.end()) {
        normals_map.emplace(point, triangle.normal);
      } else {
        pointIt->second = pointIt->second + triangle.normal;
      }
    }
  }
  for (const auto &triangle : stlModel.triangles) {
    for (const glm::vec3 &point : triangle.points()) {
      auto normal = normals_map[point];
      vertices[index++] = std::array<float, 6> {
        point[0], point[1], point[2], normal[0], normal[1], normal[2],
      };
    }
  }

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, num_vertices * sizeof(Vertex), &vertices[0],
               GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (void *)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid *)(3 * sizeof(GLfloat)));

  m_triangle_num = stlModel.triangles.size();
}

void Torus::render(const glm::mat4 &projectionMatrix,
                   const glm::mat4 &viewMatrix,
                   const glm::vec3 &lightPos,
                   const glm::vec3 &eyePos) {
  auto modelMatrix = glm::mat4(1.0f);
  modelMatrix = glm::translate(modelMatrix, m_position);
  modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5, 0.5, 0.5));
  modelMatrix = modelMatrix * rot_matrix;
  p.use();
  p.setMat4("u_mvp", projectionMatrix * viewMatrix * modelMatrix);
  p.setInt("u_hitTestRender", static_cast<int>(hitTestRender));
  p.setVec4("u_entityID", vecId());
  glBindVertexArray(vao);
  glDrawArrays(GL_TRIANGLES, 0, m_triangle_num * 3);
}