#pragma once

#include <glm/glm.hpp>
#include <gameobject.hpp>
#include "stl_model.hpp"
#include "program.hpp"

class Torus : public GameObject {
public:
  Torus(const StlModel &stlModel, const program &p, const glm::vec3 &position,
        unsigned int entityID);
  virtual ~Torus() = default;
  void render(const glm::mat4 &projectionMatrix,
              const glm::mat4 &viewMatrix,
              const glm::vec3 &lightPos,
              const glm::vec3 &eyePos) override;

private:
  unsigned int m_triangle_num;
  program p;
  glm::vec3 m_position;
};