#version 330 core

out vec4 FragColor;

in vec3 FragPos;
in vec2 TexCoord;
in vec3 Normal;

uniform vec3 u_light;
uniform vec3 u_eye;

uniform sampler2D texture1;
uniform vec4 u_entityID;
uniform int u_hitTestRender;

void hitTestRender() {
  FragColor = u_entityID;
}

void main() {
  if (u_hitTestRender == 0) {
    vec3 lightColor = vec3(1.0);
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * vec3(0.2);

    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(u_light - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    float specularStrength = 0.5;
    vec3 eyeDir = normalize(u_eye - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(eyeDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;

    vec3 result = ambient + diffuse + specular;
    FragColor = vec4(result, 1.0) * texture(texture1, TexCoord);
    // FragColor = texture(texture1, TexCoord);
  } else {
    hitTestRender();
  }
}