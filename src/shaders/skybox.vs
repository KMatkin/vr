#version 330
uniform mat4 u_mvp;
in vec3 a_position;
out vec3 v_texCoord;

void main() {
  v_texCoord = vec3(-a_position.x, -a_position.y, a_position.z);
  vec4 pos = u_mvp * vec4(a_position, 1.0);
  gl_Position = pos.xyww;
}