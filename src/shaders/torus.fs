#version 330

in vec3 ourColor;
out vec4 FragColor;

uniform vec4 u_entityID;
uniform int u_hitTestRender;

void hitTestRender() {
  FragColor = u_entityID;
}

void main() {
  if (u_hitTestRender == 0) {
    FragColor = vec4(ourColor, 1.0);
  } else {
    hitTestRender();
  }
}