#version 330 core

layout (location=0) in vec3 a_pos;
layout (location=1) in vec2 a_texCoord;
layout (location=2) in vec3 a_normal;

out vec3 FragPos;
out vec2 TexCoord;
out vec3 Normal;

uniform mat4 u_mvp;
uniform mat4 u_mv;
uniform mat4 u_m;
uniform mat4 u_n;

void main() {
  FragPos = vec3(u_m * vec4(a_pos, 1.0));
  TexCoord = vec2(a_texCoord.x, a_texCoord.y);
  Normal = mat3(u_n) * a_normal;
  gl_Position = u_mvp * vec4(a_pos, 1.0);
}