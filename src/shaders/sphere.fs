#version 330 core

in vec2 v_texCoord;
in vec3 v_fragPos;
in vec3 v_normal;
in vec3 v_ray;
in vec3 v_eye;
in vec3 v_tlight;
in vec3 v_teye;
in vec3 v_tfrag;

out vec4 FragColor;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;
uniform sampler2D texture4;
uniform sampler2D texture5;

uniform vec3 u_light;
uniform vec3 u_eye;

uniform vec4 u_entityID;
uniform int u_hitTestRender;

void renderSphere() {
  vec3 viewDir = normalize(v_teye - v_tfrag);
  vec3 normal = texture(texture2, v_texCoord).rgb;
  normal = normalize(normal * 2.0 - 1.0);
  vec3 lightColor = vec3(1.0);
  vec3 color = texture(texture1, v_texCoord).rgb;

  vec3 ambient = 0.1 * color;

  vec3 norm = normalize(normal);
  vec3 lightDir = normalize(v_tlight - v_tfrag);
  float diff = max(dot(norm, lightDir), 0.0);
  vec3 diffuse = diff * color;

  vec3 reflectDir = reflect(-lightDir, norm);
  vec3 halfwayDir = normalize(lightDir + viewDir);
  float spec = pow(max(dot(norm, halfwayDir), 0.0), 32);
  vec3 specular = vec3(0.2) * spec;

  vec3 result = ambient + diffuse + specular;
  FragColor = vec4(result, 1.0);
}

void hitTestRender() {
  FragColor = u_entityID;
}

void main() {
  if (u_hitTestRender == 0) {
    renderSphere();
  } else {
    hitTestRender();
  }
}