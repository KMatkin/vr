#version 330 core

layout (location=0) in vec3 a_pos;
layout (location=1) in vec2 a_texCoord;
layout (location=2) in vec3 a_tangent;
layout (location=3) in vec3 a_normal;

out vec2 v_texCoord;
out vec3 v_fragPos;
out vec3 v_normal;
out vec3 v_ray;
out vec3 v_eye;
out vec3 v_tlight;
out vec3 v_teye;
out vec3 v_tfrag;

uniform mat4 u_mvp;
uniform mat4 u_mv;
uniform mat4 u_m;
uniform mat4 u_n;
uniform vec3 u_light;
uniform vec3 u_eye;
uniform sampler2D texture4;
uniform int u_useDisplacement;

void main() {
  vec4 dv = texture(texture4, a_texCoord);
  float disp = (0.30 * dv.x + 0.59 * dv.y + 0.11 * dv.z) * 0.2;
  vec4 pos = vec4(a_pos, 1.0);
  if (u_useDisplacement == 1) {
    pos = vec4(a_pos + normalize(a_normal) * disp, 1.0);
  }
  v_texCoord = a_texCoord;
  // v_normal = mat3(u_n) * a_normal;
  v_fragPos = vec3(u_m * pos);
  vec3 T = normalize(mat3(u_m) * a_tangent);
  vec3 B = normalize(cross(a_normal, a_tangent));
  vec3 N = normalize(mat3(u_m) * a_normal);
  mat3 TBN = transpose(mat3(T, B, N));

  v_tlight = TBN * u_light;
  v_teye = TBN * u_eye;
  v_tfrag = TBN * v_fragPos;


  // mat3 intbn = inverse(mat3(u_n) * mat3(a_tangent, binormal, a_normal));
  // vec3 v_pos = intbn * (u_mv * pos).xyz;

  // v_ray = normalize(intbn * vec4(u_light, 1.0).xyz - v_pos);
  // v_eye = normalize(v_pos);

  gl_Position = u_mvp * pos;
}