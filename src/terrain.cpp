#include "terrain.hpp"

#include "tools.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

namespace {
const std::vector<GLuint> gen_grid_indices(const int &rows, const int &cols) {
  const int indices_num = (cols - 1) * (rows - 1) * 2 * 3;
  std::vector<GLuint> indices(indices_num);
  int index = 0;
  for (int z = 0; z < rows - 1; z++) {
    for (int x = 0; x < cols - 1; x++) {
      auto ind = x + z * cols;
      int TL = ind;
      int TR = ind + 1;
      int BL = ind + cols;
      int BR = ind + cols + 1;
      indices[index++] = TR;
      indices[index++] = BL;
      indices[index++] = BR;
      indices[index++] = TR;
      indices[index++] = TL;
      indices[index++] = BL;
    }
  }
  return indices;
}

const float XSCALE = 100.0f;
const float ZSCALE = 100.0f;
const float YSCALE = 5.0f;
const float MINX = -1.0f * XSCALE / 2;
const float MAXX = 1.0f * XSCALE / 2;
const float MINZ = -1.0f * ZSCALE / 2;
const float MAXZ = 1.0f * ZSCALE / 2;

} // namespace

Terrain::Terrain(const program &p, const std::string &height_map_path,
                 const std::string &texture_path, unsigned int entityID)
    : GameObject(entityID) {
  this->p = p;
  auto height_map = Image{height_map_path};
  auto texture = Image{texture_path};
  auto num_vertices = height_map.width * height_map.height;
  this->m_width = height_map.width;
  this->m_height = height_map.height;
  m_vertices = std::vector<Vertex>(num_vertices);
  float scale = m_width > m_height ? 1.0f / m_width : 1.0f / m_height;
  for (size_t i = 0; i < m_height; i++) {
    for (size_t j = 0; j < m_width; j++) {
      size_t ind = j + i * m_width;
      float grey = static_cast<float>(height_map.grey({i, j})) /
                   height_map.max_component_value;
      float x = (j * 1.0f - m_width / 2) * scale;
      float y = grey;
      float z = (i * 1.0f - m_height / 2) * scale;
      float u = j * 1.0f / 10;
      float v = i * 1.0f / 10;
      m_vertices[ind] = Vertex{
          x, y, z, u, v, 0.0f, 0.0f, 0.0f,
      };
    }
  }

  auto indices = gen_grid_indices(m_height, m_width);
  m_indices_num = indices.size();

  for (size_t i = 0; i < m_height; i++) {
    for (size_t j = 0; j < m_width; j++) {
      size_t ind = j + i * m_width;
      #define vector(a, b) glm::vec3((b)[0] - (a)[0], (b)[1] - (a)[1], (b)[2] - (a)[2])
      glm::vec3 topLeftNormal = glm::vec3(0.0f);
      glm::vec3 topRightNormal = glm::vec3(0.0f);
      glm::vec3 bottomLeftNormal = glm::vec3(0.0f);
      glm::vec3 bottomRightNormal = glm::vec3(0.0f);
      Vertex currentVertex = m_vertices[ind];
      Vertex topVertex { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      Vertex leftVertex { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      Vertex rightVertex { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      Vertex bottomVertex { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      if (i != 0) {
        topVertex = m_vertices[ind - m_width];
      }
      if (j != 0) {
        leftVertex = m_vertices[ind - 1];
      }
      if (j != m_width - 1) {
        rightVertex = m_vertices[ind + 1];
      }
      if (i != m_height - 1) {
        bottomVertex = m_vertices[ind + m_width];
      }
      auto toTop = vector(currentVertex, topVertex);
      auto toLeft = vector(currentVertex, leftVertex);
      auto toRight = vector(currentVertex, rightVertex);
      auto toBottom = vector(currentVertex, bottomVertex);
      topLeftNormal = glm::normalize(glm::cross(toTop, toLeft));
      topRightNormal = glm::normalize(glm::cross(toRight, toTop));
      bottomLeftNormal = glm::normalize(glm::cross(toLeft, toBottom));
      bottomRightNormal = glm::normalize(glm::cross(toBottom, toRight));
      auto normal = glm::normalize(topLeftNormal + topRightNormal + bottomLeftNormal + bottomRightNormal);
      m_vertices[ind][5] = normal.x;
      m_vertices[ind][6] = normal.y;
      m_vertices[ind][7] = normal.z;
    }
  }

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);

  float *d = static_cast<float *>(&m_vertices[0][0]);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(Vertex), d,
               GL_STATIC_DRAW);

  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices_num * sizeof(GLuint),
               &(indices[0]), GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        (const GLvoid *)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 
                  (const GLvoid*)(5 * sizeof(GLfloat)));

  glGenTextures(1, &texture1);
  glBindTexture(GL_TEXTURE_2D, texture1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
#ifdef GL_EXT_texture_filter_anisotropic
  GLfloat fLargest;
  glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);
#endif
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture.width, texture.height, 0,
               GL_RGB, GL_UNSIGNED_BYTE, texture.data);
  glGenerateMipmap(GL_TEXTURE_2D);
}

float Terrain::height(float x, float z) const {
  float relz = (z + (MAXZ - MINZ) / 2) / (MAXZ - MINZ);
  float relx = (x + (MAXZ - MINX) / 2) / (MAXX - MINX);
  int i = static_cast<int>(relz * (this->m_height - 1));
  int j = static_cast<int>(relx * (this->m_width - 1));
  float fi = (relz * this->m_height) - static_cast<float>(i);
  float fj = (relx * this->m_width) - static_cast<float>(j);
  float nfi = 1.0f - fi;
  float nfj = 1.0f - fj;
  return ((heightAt(i, j) * nfi + heightAt(i + 1, j) * fi) * nfj +
          (heightAt(i, j + 1) * nfi + heightAt(i + 1, j + 1) * fi) * fj);
}

float Terrain::heightAt(unsigned int i, unsigned int j) const {
  if (i >= m_height) {
    i = m_height - 1;
  }
  if (j >= m_width) {
    j = m_width - 1;
  }
  return this->m_vertices[j + m_width * i][1];
}

void Terrain::contraint(glm::vec3 &position, const float h) const {
  if (position.x < MINX)
    position.x = MINX;
  else if (position.x > MAXX)
    position.x = MAXX;

  if (position.z < MINZ)
    position.z = MINZ;
  else if (position.z > MAXZ)
    position.z = MAXZ;
  position.y = h + YSCALE * height(position.x, position.z);
}

void Terrain::render(const glm::mat4 &projectionMatrix,
                     const glm::mat4 &viewMatrix,
                     const glm::vec3 &lightPos,
                     const glm::vec3 &eyePos) {
  const auto ident = glm::mat4(1.0f);
  const auto modelMatrix = glm::scale(ident, glm::vec3(XSCALE, YSCALE, ZSCALE));
  const auto normalMatrix = glm::transpose(glm::inverse(modelMatrix));
  p.use();
  const auto texUnit = 0;
  glActiveTexture(GL_TEXTURE0 + texUnit);
  glBindTexture(GL_TEXTURE_2D, texture1);
  p.setInt("texture1", 0);
  auto modelViewMatrix = viewMatrix * modelMatrix;
  auto mvp = projectionMatrix * modelViewMatrix;
  p.setMat4("u_mvp", mvp);
  p.setMat4("u_mv", modelViewMatrix);
  p.setMat4("u_m", modelMatrix);
  p.setMat4("u_n", normalMatrix);
  p.setVec3("u_eye", eyePos);
  p.setVec3("u_light", lightPos);
  p.setInt("u_hitTestRender", static_cast<int>(hitTestRender));
  p.setVec4("u_entityID", vecId());
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, m_indices_num, GL_UNSIGNED_INT, 0);
}
