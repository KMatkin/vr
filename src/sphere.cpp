#include "sphere.hpp"
#include <array>
#include <vector>
#include "resources.hpp"
#include "image.hpp"
#include "tools.hpp"

using namespace resources;

Sphere::Sphere(const program &p, const glm::vec3 &position, unsigned int entityID)
  : GameObject(entityID), p(p), m_position(position) {
  using Vertex = std::array<float, 11>;
  using std::sin;
  using std::cos;
  using std::size_t;

  const auto radius = 1.0f;
  const auto meridians = 39;
  const auto parallels = 25;
  (void)radius;
  (void)meridians;
  (void)parallels;

  glm::vec3 nm(0.0, 0.0, 1.0);
  std::vector<Vertex> vertices;
  for (size_t i = 0; i <= parallels; i++) {
      float theta = i * M_PI / parallels;
      float sinTheta = sin(theta);
      float cosTheta = cos(theta);

      for (size_t j = 0; j <= meridians; j++) {
        float phi = j * 2 * M_PI / meridians;
        float cosPhi = cos(phi);
        float sinPhi = sin(phi);
        float z = radius * sinPhi * sinTheta; // x in cartesian corrdinate system
        float x = radius * cosPhi * sinTheta; // y in cartesian coordinate system
        float y = radius * cosTheta;          // z in cartesian coordinate system
        float u = 1 - (float)j / meridians;
        float v = 1 - (float)i / parallels;
        vertices.push_back(Vertex {
          x, y, z, u, v, 0.0f, 0.0f, 0.0f, x, y, z
        });
      }
  }

  std::vector<unsigned int> indices;
  indices_num = 2 * meridians * 3 + meridians * (parallels - 2) * 6;
  indices.resize(indices_num);
  auto it = indices.begin();
  for (size_t i = 0; i < parallels; i++) {
    for (size_t j = 0; j < meridians; j++) {
      if (i != 0) {
        *it++ = i * (meridians+1) + j;
        *it++ = i * (meridians+1) + j + 1;
        *it++ = (i + 1) * (meridians+1) + j + 1;
      }
      if (i != parallels - 1) {
        *it++ = (i + 1) * (meridians+1) + j + 1;
        *it++ = (i + 1) * (meridians+1) + j;
        *it++ = i * (meridians+1) + j;
      }
    }
  } 

/*
  vertices.push_back(Vertex {
    -1.0, 1.0, 0.0,  0.0, 1.0, 0.0, 0.0, 0.0, nm.x, nm.y, nm.z,
  });
  vertices.push_back(Vertex{
    -1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,nm.x, nm.y, nm.z,
  });
  vertices.push_back(Vertex{
    1.0, -1.0, 0.0,  1.0, 0.0, 0.0, 0.0, 0.0,nm.x, nm.y, nm.z,
  });
  vertices.push_back(Vertex{
    1.0, 1.0, 0.0,   1.0, 1.0, 0.0, 0.0, 0.0, nm.x, nm.y, nm.z,
  });

 std::vector<unsigned int> indices {
   0, 1, 2, 2, 3, 0
 };
 indices_num = 6;
  */
#define ind(nmb) (indices[i + (nmb)])
  for (size_t i = 0; i < indices_num; i += 3) {
    glm::vec3 eo1 = {
      vertices[ind(1)][0] - vertices[ind(0)][0],
      vertices[ind(1)][1] - vertices[ind(0)][1],
      vertices[ind(1)][2] - vertices[ind(0)][2]
    };
    glm::vec3 eo2 = {
      vertices[ind(2)][0] - vertices[ind(0)][0],
      vertices[ind(2)][1] - vertices[ind(0)][1],
      vertices[ind(2)][2] - vertices[ind(0)][2]
    };
    glm::vec2 et1 = {
      vertices[ind(1)][3] - vertices[ind(0)][3],
      vertices[ind(1)][4] - vertices[ind(0)][4],
    };
    glm::vec2 et2 = {
      vertices[ind(2)][3] - vertices[ind(0)][3],
      vertices[ind(2)][4] - vertices[ind(0)][4],
    };

    float f = 1.0f / (et1.x * et2.y - et2.x * et1.y);

    glm::vec3 tangent {
      f * (et2.y * eo1.x - et1.y * eo2.x),
      f * (et2.y * eo1.y - et1.y * eo2.y),
      f * (et2.y * eo1.z - et1.y * eo2.z)
    };
    if (glm::length(tangent) == 0.0f) {
      std::cout << "tangent is zero! " << i << std::endl;
    }
    tangent = glm::normalize(tangent);
    for (size_t j = 0; j < 3; j++) {
      glm::vec3 t = {
        vertices[ind(j)][5],
        vertices[ind(j)][6],
        vertices[ind(j)][7],
      };
      t += tangent;
      t = glm::normalize(t);
      vertices[ind(j)][5] = t.x;
      vertices[ind(j)][6] = t.y;
      vertices[ind(j)][7] = t.z;
    }
  }

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), static_cast<const void*>(&vertices[0][0]), GL_STATIC_DRAW);

  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)(0));
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)(5 * sizeof(GLfloat)));
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)(8 * sizeof(GLfloat)));


  #define processTexture(path, variable, name) {\
    auto tex = Image { path }; \
    glGenTextures(1, &variable); \
    glBindTexture(GL_TEXTURE_2D, variable); \
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT); \
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT); \
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); \
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); \
    glTexImage2D(GL_TEXTURE_2D, 0, tex.loaded_channels == 3 ? GL_RGB : GL_RGBA, tex.width, tex.height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex.data); }

  processTexture(images::sphere_basecolor(), texture1, "baseColorTexture");
  processTexture(images::sphere_normal_map(), texture2, "normalTexture");
  processTexture(images::sphere_ambient_map(), texture3, "baseColorTexture");
  processTexture(images::sphere_height_map(), texture4, "heightMapTexture");
  processTexture(images::sphere_roughness_map(), texture5, "roughnessTexture");
}

void Sphere::render(const glm::mat4 &projectionMatrix,
              const glm::mat4 &viewMatrix,
              const glm::vec3 &lightPos,
              const glm::vec3 &eyePos) {
  auto modelMatrix = glm::mat4(1.0f);
  modelMatrix = glm::translate(modelMatrix, m_position);
  modelMatrix = modelMatrix * rot_matrix;
  auto normalMatrix = glm::transpose(glm::inverse(modelMatrix));
  auto modelViewMatrix = viewMatrix * modelMatrix;
  auto mvp = projectionMatrix * modelViewMatrix;
  p.use();
  glActiveTexture(GL_TEXTURE0 + 0);
  glBindTexture(GL_TEXTURE_2D, texture1);
  glActiveTexture(GL_TEXTURE0 + 1);
  glBindTexture(GL_TEXTURE_2D, texture2);
  glActiveTexture(GL_TEXTURE0 + 2);
  glBindTexture(GL_TEXTURE_2D, texture3);
  glActiveTexture(GL_TEXTURE0 + 3);
  glBindTexture(GL_TEXTURE_2D, texture4);
  glActiveTexture(GL_TEXTURE0 + 4);
  glBindTexture(GL_TEXTURE_2D, texture5);
  p.setMat4("u_mvp", mvp);
  p.setMat4("u_m", modelMatrix);
  p.setMat4("u_n", normalMatrix);
  p.setMat4("u_mv", modelViewMatrix);
  p.setInt("texture1", 0);
  p.setInt("texture2", 1);
  p.setInt("texture3", 2);
  p.setInt("texture4", 3);
  p.setInt("texture5", 4);
  p.setInt("u_useDisplacement", static_cast<int>(m_useDisplacement));
  p.setVec3("u_light", lightPos);
  p.setVec3("u_eye", eyePos);
  p.setInt("u_hitTestRender", static_cast<int>(hitTestRender));
  p.setVec4("u_entityID", vecId());
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, indices_num, GL_UNSIGNED_INT, 0);
}

bool Sphere::processInput(GLFWwindow *window) {
    auto currentState = glfwGetKey(window, GLFW_KEY_T);
    if (currentState < GLFW_PRESS && currentState != m_lastTState) {
      m_useDisplacement = !m_useDisplacement;
    }
    m_lastTState = currentState;
    return GameObject::processInput(window);
  }
