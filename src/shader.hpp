#pragma once

#include <GLFW/glfw3.h>
#include <exception>
#include <fstream>
#include <sstream>
#include <string>

struct shader_failure : std::ifstream::failure {
  using std::ifstream::failure::failure;
};

template <GLuint Type> struct shader {
  GLuint pointer;

  shader(const std::string &path) {
    std::ifstream file;
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    std::string source;
    try {
      file.open(path);
      std::stringstream ss;
      ss << file.rdbuf();
      source = ss.str();
      file.close();
    } catch (std::ifstream::failure e) {
      throw shader_failure{"Error: couldn't load shader at " + path};
    }

    const char *sourceChar = source.c_str();
    pointer = glCreateShader(Type);
    glShaderSource(pointer, 1, &sourceChar, NULL);
    glCompileShader(pointer);

    GLint success;
    char infoLog[512];
    glGetShaderiv(pointer, GL_COMPILE_STATUS, &success);
    if (!success) {
      glGetShaderInfoLog(pointer, 512, NULL, infoLog);
      throw shader_failure{"Compilation error: " + std::string(infoLog)};
    }
  }

  ~shader() { glDeleteShader(pointer); }
};

using vertex_shader = shader<GL_VERTEX_SHADER>;
using fragment_shader = shader<GL_FRAGMENT_SHADER>;