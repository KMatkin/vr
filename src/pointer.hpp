#pragma once

#include "program.hpp"
#include <gameobject.hpp>

class Pointer : public GameObject {
public:
  Pointer(program p); 
  virtual ~Pointer() = default;
  void render(const glm::mat4 &proj, const glm::mat4 &view, const glm::vec3 &lightPos, const glm::vec3 &eyePos) override;

private:
  program p;
};