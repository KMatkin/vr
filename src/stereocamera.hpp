#pragma once

#include <glm/glm.hpp>
#include "camera.hpp" 

class StereoCamera: public Camera {
public:
  StereoCamera(float convergence, float eyeSeparation, 
               const glm::vec3 &position = glm::vec3(0.0, 0.0, 0.0),
               const CameraConstraint &constraint = defaultConstraint,
               double fov = glm::radians(45.0f),
               float near = 0.01f,
               float far = 100.0f)
    : Camera(position, constraint, fov, near ,far), convergence(convergence), eyeSeparation(eyeSeparation) {}

  glm::mat4 rightFrustum() const;
  glm::mat4 leftFrustum() const;
  glm::mat4 stereoViewMatrix(bool isLeft);
       
  float convergence;
  float eyeSeparation;

private:
  glm::mat4 calcFrustum(bool isLeft) const;
};