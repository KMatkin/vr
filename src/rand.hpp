#pragma once
#include <stdlib.h>

inline float rand_float() {
  return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}