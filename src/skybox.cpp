#include "skybox.hpp"
#include "image.hpp"
#include "tools.hpp"

Skybox::Skybox(const program &p, const std::string &texture_directory,
               unsigned int entityID)
    : GameObject(entityID), p(p) {
  const GLfloat vertices[] = {
      -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f,  1.0f,  1.0f,
      1.0f,  -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f,
      -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f,  -1.0f,
  };
  const GLuint indices[] = {
      0, 3, 1, 1, 3, 2, 0, 4, 7, 7, 3, 0, 1, 2, 6, 6, 5, 1,
      5, 6, 7, 7, 4, 5, 3, 7, 6, 6, 2, 3, 0, 1, 5, 5, 4, 0,
  };

  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);

  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
               GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3,
                        (void *)0);

  glGenTextures(1, &handler);
  glBindTexture(GL_TEXTURE_CUBE_MAP, handler);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  auto left = Image{texture_directory + "/left.jpg"};
  auto right = Image{texture_directory + "/right.jpg"};
  auto top = Image{texture_directory + "/top.jpg"};
  auto bottom = Image{texture_directory + "/bottom.jpg"};
  auto back = Image{texture_directory + "/back.jpg"};
  auto front = Image{texture_directory + "/front.jpg"};
#define loadCubic(side, image)                                                 \
  glTexImage2D(side, 0, GL_RGB, image.width, image.height, 0, GL_RGB,          \
               GL_UNSIGNED_BYTE, image.data);
  loadCubic(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, left);
  loadCubic(GL_TEXTURE_CUBE_MAP_POSITIVE_X, right);
  loadCubic(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, top);
  loadCubic(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, bottom);
  loadCubic(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, back);
  loadCubic(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, front);
}

void Skybox::render(const glm::mat4 &projectionMatrix,
                    const glm::mat4 &viewMatrix,
                    const glm::vec3 &lightPos,
                    const glm::vec3 &eyePos) {
  auto mvp = projectionMatrix * remove_translation(viewMatrix);
  glDepthFunc(GL_LEQUAL);
  p.use();
  p.setMat4("u_mvp", mvp);
  const auto texUnit = 0;
  p.setInt("u_map", texUnit);
  glBindVertexArray(vao);
  glActiveTexture(GL_TEXTURE0 + texUnit);
  glBindTexture(GL_TEXTURE_CUBE_MAP, handler);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  glDepthFunc(GL_LESS);
}
