#pragma once

#include <glm/glm.hpp>
#include <iomanip>
#include <ostream>

template <typename T, glm::qualifier Q, glm::length_t N, glm::length_t M>
inline std::ostream &operator<<(std::ostream &out,
                                const glm::mat<N, M, T, Q> &mat) {
  out << "[" << std::endl;
  for (size_t i = 0; i < N; i++) {
    out << " [";
    for (size_t j = 0; j < M; j++) {
      out << std::setw(6) << std::setprecision(4) << mat[i][j] << ",";
    }
    out << "]" << std::endl;
  }
  out << "]";
  return out;
}

template<typename T, size_t L>
inline std::ostream &operator<<(std::ostream &out, const std::array<T, L> &arr) {
  out << "[";
  for (size_t i = 0; i < L; i++) {
    out << std::setw(6) << std::setprecision(4) << arr[i] << ",";
  }
  out << "]";
  return out;
}

template <typename T, glm::qualifier Q, glm::length_t L>
inline std::ostream &operator<<(std::ostream &out,
                                const glm::vec<L, T, Q> &vec) {
  out << "[";
  for (size_t i = 0; i < L; i++) {
    out << std::setw(6) << std::setprecision(4) << vec[i] << ",";
  }
  out << "]";
  return out;
}

inline glm::mat4 remove_translation(const glm::mat4 &mat) {
  return glm::mat4(glm::mat3(mat));
}
