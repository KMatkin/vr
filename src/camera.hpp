#pragma once

#include <functional>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

using CameraConstraint = std::function<void(glm::vec3 &, const float)>;
const auto defaultConstraint = [](auto &pos, auto height) {};

class Camera {
public:
  Camera(const glm::vec3 &position = glm::vec3(0.0f, 0.0f, 0.0f),
         const CameraConstraint &constraint = defaultConstraint,
         double fov = glm::radians(45.0f),
         float near = 0.01f,
         float far = 100.0f)
      : m_position(std::move(position)), m_contraint(std::move(constraint)), m_fov(fov), m_near(near), m_far(far) {}

  void move(float delta);
  void strafe(float delta);
  void yaw(float angle);
  void pitch(float angle);
  void updateProjection(unsigned int screenWidth, unsigned int screenHeight);
  glm::mat4 viewMatrix() const;
  glm::mat4 projectionMatrix() const;
  void processInput(GLFWwindow* window);
  glm::vec3 position() const { return m_position; }

  static constexpr float Height = 0.5f;

protected:
  glm::vec3 m_position = {0.0f, 0.0f, 0.0f};
  glm::vec3 m_orientation = {0.0f, 0.0f, 0.0f};
  CameraConstraint m_contraint;
  float m_fov;
  float m_near;
  float m_far;
  float m_aspect = 800.0/600.0;
  glm::mat4 m_projection;
};