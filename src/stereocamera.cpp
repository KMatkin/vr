#include "stereocamera.hpp"

#include <glm/gtc/matrix_transform.hpp>


glm::mat4 StereoCamera::rightFrustum() const {
  return calcFrustum(false);
}

glm::mat4 StereoCamera::leftFrustum() const { 
  return calcFrustum(true);
}

glm::mat4 StereoCamera::stereoViewMatrix(bool isLeft) {
  glm::vec3 centerPosition = m_position;
  m_position.x = m_position.x + (isLeft ? 1 : -1) * eyeSeparation / 2;
  auto result = Camera::viewMatrix();
  m_position.x = centerPosition.x;
  return result;
}

glm::mat4 StereoCamera::calcFrustum(bool isLeft) const {
    using std::tan;
    float top, bottom, left, right;

    top = m_near * tan(m_fov / 2);
    bottom = -top;

    float a = m_aspect * tan(m_fov / 2) * convergence;
    float b = a - eyeSeparation / 2;
    float c = a + eyeSeparation / 2;

    if (isLeft) {
      left = -b * m_near / convergence;
      right = c * m_near / convergence;
    } else {
      left = -c * m_near / convergence;
      right = b * m_near / convergence;
    }
    return glm::frustum(left, right, bottom, top, m_near, m_far);
  }
