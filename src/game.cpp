#include "game.hpp"
#include "resources.hpp"
#include "terrain.hpp"
#include "skybox.hpp"
#include "torus.hpp"
#include "camera.hpp"
#include "image.hpp"
#include "sphere.hpp"
#include "tools.hpp"
#include "stereocamera.hpp"

using namespace resources;

Game::Game(GLFWwindow *window, std::shared_ptr<Screen> screen) 
  : m_window(window), m_scene(std::make_unique<Scene>(screen)),
    m_screen(screen), m_cursor(Cursor {}) {

  auto entityManager = std::make_unique<EntityManager>();
  auto terrain = entityManager->create<Terrain>(
    program{
      vertex_shader{shaders::terrain_vs()},
      fragment_shader{shaders::terrain_fs()}
    },
    images::height_map(), 
    images::terrain()
  );
  auto terrainConstraint = [terrain](auto &position, auto height) {
    terrain->contraint(position, height);
  };

  auto skybox = std::make_unique<Skybox>(
    program{
        vertex_shader{shaders::skybox_vs()},
        fragment_shader{shaders::skybox_fs()},
    },
    images::skybox(), 
    0
  );

  auto initialCameraPosition = glm::vec3(0.0, 0.0, 3.0f);
  terrain->contraint(initialCameraPosition, Camera::Height);

  auto camera = std::make_unique<StereoCamera>(
    20.0f,
    -0.1f,
    initialCameraPosition, 
    terrainConstraint,
    glm::radians(45.0f),
    0.01,
    100.0
  );
  camera->updateProjection(m_screen->width, m_screen->height);

  auto initialTorusPosition = glm::vec3(0.0, 0.0, 0.0);
  terrain->contraint(initialTorusPosition, 1);

  auto torus = entityManager->create<Torus>(
    load_stl_model_from_file(models::torus()),
    program{
        vertex_shader{shaders::torus_vs()},
        fragment_shader{shaders::torus_fs()},
    },
    initialTorusPosition
  );

  auto pointer = std::make_unique<Pointer>(
    program{
      vertex_shader{shaders::pointer_vs()},
      fragment_shader{shaders::pointer_fs()}
    });
  auto sphereShader = program{
    vertex_shader{shaders::sphere_vs()},
    fragment_shader{shaders::sphere_fs()}
  };

  for (int i = 0; i < 5; i++) {
    auto initialSpherePosition = glm::vec3(3.0 + i * 2.0, 0.0, 0.0);
    terrain->contraint(initialSpherePosition, 1);
    auto sphere = entityManager->create<Sphere>(
            sphereShader,
            initialSpherePosition
    );
    m_scene->addObject(sphere);
  }

  m_scene->addObject(torus);
  m_scene->addObject(terrain);
  m_scene->skybox = std::move(skybox);
  m_scene->camera = std::move(camera);
  m_scene->entityManager = std::move(entityManager);
  m_scene->pointer = std::move(pointer);
  m_scene->lightPos = glm::vec3(0.0f, 4.417f, 0.0f);
}

void Game::processInput() {
  if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
    shouldClose = true;
    return;
  }
  m_scene->processInput(m_window);
}

void Game::update() {
  m_scene->update();
}

void Game::render() {
  m_scene->render();
  glfwSwapBuffers(m_window);
}

void Game::onFramebufferSizeChanged(int w, int h) {
  glViewport(0, 0, w, h);
  auto width = static_cast<unsigned int>(w);
  auto height = static_cast<unsigned int>(h);
  m_screen->width = width;
  m_screen->height = height;
  m_scene->camera->updateProjection(width, height);
}

void Game::onMousePositionChanged(double xpos, double ypos) {
  if (m_cursor.firstMove) {
    m_cursor = Cursor { xpos, ypos };
    m_cursor.firstMove = false;
    return;
  }
  float dx = xpos - m_cursor.xpos;
  float dy = ypos - m_cursor.ypos;
  m_cursor = Cursor { xpos, ypos };

  if (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) return;
  const float mouseSensitive = 0.01f;
  dx *= mouseSensitive;
  dy *= mouseSensitive;

  m_scene->camera->yaw(dx);
  m_scene->camera->pitch(dy);
}

void Game::onKeyModeChanged(int key, int scancode, int action, int mods) {
}