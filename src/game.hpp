#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <memory>
#include <glm/glm.hpp>
#include "screen.hpp"
#include "scene.hpp"

const int InitialWidth = 800;
const int InitialHeight = 600;
const std::string GameTitle = "VR";

class Game {
 public:

  struct Cursor {
    double xpos;
    double ypos;
    bool firstMove;

    Cursor(double xpos, double ypos) {
      this->firstMove = false;
      this->xpos = xpos;
      this->ypos = ypos;
    }
    Cursor() { 
      this->firstMove = true;
    }
  };

  Game(GLFWwindow *window, std::shared_ptr<Screen> screen = std::make_shared<Screen>(InitialWidth, InitialHeight));

  void processInput();
  void update();
  void render();
  void onFramebufferSizeChanged(int width, int height);
  void onMousePositionChanged(double xpos, double ypos);
  void onKeyModeChanged(int key, int scanmode, int action, int mods);

  bool shouldClose = false;

 private:
  GLFWwindow* m_window;
  std::unique_ptr<Scene> m_scene;
  std::shared_ptr<Screen> m_screen;
  Cursor m_cursor;
};