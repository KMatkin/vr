#pragma once

#include <array>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <gameobject.hpp>
#include <string>
#include <vector>
#include "image.hpp"
#include "program.hpp"
#include "rand.hpp"

class Terrain : public GameObject {
public:
  using Vertex = std::array<float, 3 + 2 + 3>;


  Terrain(const program &p, const std::string &height_map_path,
          const std::string &texture_path, unsigned int entityID);

  void contraint(glm::vec3 &position, const float h) const;
  float height(float x, float z) const;
  float heightAt(unsigned int i, unsigned int j) const;

  void render(const glm::mat4 &projectionMatrix,
              const glm::mat4 &viewMatrix,
              const glm::vec3 &lightPos,
              const glm::vec3 &eyePos) override;
  virtual ~Terrain() = default;

private:
  program p;
  std::vector<Vertex> m_vertices;
  unsigned int m_indices_num;
  unsigned int m_width;
  unsigned int m_height;
  GLuint texture1;
};