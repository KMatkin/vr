#pragma once

#include <glad/glad.h>
#include "program.hpp"
#include <glm/glm.hpp>
#include <gameobject.hpp>
#include <vector>

class Skybox : public GameObject {
public:
  Skybox(const program &p, const std::string &texture_directory,
         unsigned int entityID);
  virtual ~Skybox() = default;
  void render(const glm::mat4 &projectionMatrix,
              const glm::mat4 &viewMatrix,
              const glm::vec3 &lightPos,
              const glm::vec3 &eyePos) override;

private:
  GLuint handler;
  program p;
};