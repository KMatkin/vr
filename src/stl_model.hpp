#pragma once

#include <array>
#include <fstream>
#include <glm/glm.hpp>
#include <vector>

struct StlModel {
  struct Triangle {
    glm::vec3 normal;
    glm::vec3 v1;
    glm::vec3 v2;
    glm::vec3 v3;

    std::array<glm::vec3, 3> points() const { return {v1, v2, v3}; }
  };

  std::vector<Triangle> triangles;
};

inline StlModel load_stl_model_from_file(const std::string &filename) {
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (!in) {
    std::cerr << "Couldn't open model file " << filename << std::endl;
    assert(false);
  }

  StlModel model = {};
  char header[80];
  unsigned int triangleNum;
  char attributeByteCount[2];
  in.read(header, sizeof(header));
  in.read(reinterpret_cast<char *>(&triangleNum), sizeof(triangleNum));
  model.triangles = std::vector<StlModel::Triangle>(triangleNum);
  for (unsigned int i = 0; i < triangleNum; i++) {
    StlModel::Triangle triangle;
    for (int c = 0; c < 3; c++) {
      in.read(reinterpret_cast<char *>(&triangle.normal[c]), sizeof(float));
    }
    for (int c = 0; c < 3; c++) {
      in.read(reinterpret_cast<char *>(&triangle.v1[c]), sizeof(float));
    }
    for (int c = 0; c < 3; c++) {
      in.read(reinterpret_cast<char *>(&triangle.v2[c]), sizeof(float));
    }
    for (int c = 0; c < 3; c++) {
      in.read(reinterpret_cast<char *>(&triangle.v3[c]), sizeof(float));
    }
    in.read(attributeByteCount, sizeof(attributeByteCount));
    model.triangles[i] = triangle;
  }
  return model;
}