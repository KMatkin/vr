#include "camera.hpp"

#include "tools.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <math.h>

inline float wrap(float a, float min, float max) {
  a -= min;
  max -= min;
  if (max == 0)
    return min;
  a = static_cast<float>(fmod(a, max)) + min;
  if (a < min)
    a += max;
  return a;
}

void Camera::move(float delta) {
  m_position.x -= std::sin(m_orientation.y) * delta;
  m_position.z -= std::cos(m_orientation.y) * delta;
  m_contraint(m_position, Height);
}

void Camera::strafe(float delta) {
  float a = m_orientation.y - M_PI / 2.0f;
  m_position.x -= std::sin(a) * delta;
  m_position.z -= std::cos(a) * delta;
  m_contraint(m_position, Height);
}

void Camera::yaw(float angle) {
  m_orientation.y = wrap(m_orientation.y - angle, 0.0f, 2.0f * M_PI);
}

void Camera::pitch(float angle) {
  m_orientation.x = wrap(m_orientation.x - angle, 0.0f, 2.0f * M_PI);
}

glm::mat4 Camera::viewMatrix() const {
  // std::cout << "Camera position " << m_position << std::endl
  // << "Camera orientitation " << m_orientation << std::endl;
  auto glm_result = glm::mat4(1.0f);
  glm_result =
      glm::rotate(glm_result, -m_orientation.x, glm::vec3(1.0, 0.0, 0.0));
  glm_result =
      glm::rotate(glm_result, -m_orientation.y, glm::vec3(0.0, 1.0, 0.0));
  glm_result =
      glm::rotate(glm_result, -m_orientation.z, glm::vec3(0.0, 0.0, 1.0));
  glm_result = glm::translate(glm_result, -m_position);
  return glm_result;
}

void Camera::updateProjection(unsigned int screenWidth, unsigned int screenHeight) {
  m_aspect = (float)screenWidth/(float)screenHeight;
  m_projection = glm::perspective(m_fov, m_aspect, m_near, m_far);
}

glm::mat4 Camera::projectionMatrix() const {
  return m_projection;
}

void Camera::processInput(GLFWwindow* window) {
  const float step = 0.02f;
  const float rotateAngle = -glm::radians(1.0f);
  float accelCoefficient = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS ? 5.0f : 1.0f;
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
    move(step * accelCoefficient);
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
    move(-step);
  }
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
    yaw(rotateAngle);
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
    yaw(-rotateAngle);
  }
}