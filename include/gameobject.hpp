#pragma once

#include <iostream>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <tuple>
#include "rgbcolor.hpp"

class GameObject {
public:
  GameObject(unsigned int entityID) : id(entityID) {}
  virtual void render(const glm::mat4 &projectionMatrix,
                      const glm::mat4 &viewMatrix,
                      const glm::vec3 &lightPos,
                      const glm::vec3 &eyePos) = 0;
  virtual void update(double delta) {};
  unsigned int getId() const { return this->id; };

  inline RGBColor rgbId() const {
    return RGBColor {
      (id & 0x000000FF) >> 0,
      (id & 0x0000FF00) >> 8,
      (id & 0x00FF0000) >> 16,
    };
  }

  virtual bool processInput(GLFWwindow* window) {
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    if (firstMouseMove) {
      lastX = xpos;
      lastY = ypos;
      firstMouseMove = false;
      return true;
    }
    float dx = static_cast<float>(xpos) - lastX;
    float dy = static_cast<float>(ypos) - lastY;
    lastX = xpos;
    lastY = ypos;
    dragChanged(dx, dy);
    return false;
  }
 
  inline glm::vec4 vecId() const {
    auto rgbColor = rgbId();
    return glm::vec4(rgbColor.r/255.0f, rgbColor.g/255.0f, rgbColor.b/255.0f, 1.0f);
  }

  bool hitTestRender = false;
  bool firstMouseMove = true;
  float lastX;
  float lastY;

protected:
  unsigned int id;
  GLuint vao;
  GLuint vbo;
  GLuint ibo;
  GLuint texture1;
  GLuint texture2;
  GLuint texture3;
  GLuint texture4;
  GLuint texture5;
  glm::mat4 rot_matrix = glm::mat4{1.0f};

private:
  void dragChanged(float dx, float dy) {
    const float rotSpeed = M_PI / 600;
    glm::mat4 r = rot_matrix;
    r = glm::rotate(r, dx * rotSpeed, glm::vec3(r[0][1], r[1][1], r[2][1]));
    r = glm::rotate(r, dy * rotSpeed, glm::vec3(r[0][0], r[1][0], r[2][0]));
    normalize(r);
    rot_matrix = r;
  }

  inline void normalize(glm::mat4 &mat) {
    float vecLengths[3] = {
      glm::length(glm::vec3(mat[0][0], mat[1][0], mat[2][0])),
      glm::length(glm::vec3(mat[0][1], mat[1][1], mat[2][1])),
      glm::length(glm::vec3(mat[0][2], mat[1][2], mat[2][2]))
    };
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        mat[i][j] /= vecLengths[j];
      }
    }
  }
 

};