file(GLOB 
  ALL_CXX_SOURCE_FILES 
  ${PROJECT_SOURCE_DIR}/src/*.[chi]pp 
  ${PROJECT_SOURCE_DIR}/*.[chi]xx 
  ${PROJECT_SOURCE_DIR}/*.cc 
  ${PROJECT_SOURCE_DIR}/*.hh 
  ${PROJECT_SOURCE_DIR}/*.ii 
  ${PROJECT_SOURCE_DIR}/*.[CHI]
)

find_program(CLANG_FORMAT "clang-format")
if(CLANG_FORMAT)
  add_custom_target(
    clang-format
    COMMAND /usr/bin/clang-format
    -i
    -style=file
    ${ALL_CXX_SOURCE_FILES}
  )
endif()

find_program(CLANG_TIDY "clang-tidy")
if(CLANG_TIDY)
  add_custom_target(
    clang-tidy
    COMMAND /usr/bin/clang-tidy
    ${ALL_CXX_SOURCE_FILES}
    -config=''
    --
    --std=c++17
    ${INCLUDE_DIRECTORIES}
  )
endif()